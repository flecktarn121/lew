
import xml.etree.ElementTree as ET
import lxml.etree as etree
import argparse, re

def parse_XML(filename):
    ''' Given an filename for the XML, return the root
        Args:
            filename (str): the name of the XML file
        Returns:
            ElementTree.Element: the root of the XML
    '''
    try:
        tree = ET.parse(filename)
        return tree.getroot()
    except FileNotFoundError:
        print("No file found under name " + filename)
        exit(-1)

def processDuration(duration):
    hours = re.findall("([0-9]*)H", duration)
    minutes = re.findall("([0-9]*)M", duration)
    seconds = re.findall("([0-9]*)S", duration)
    
    hoursText = "0" if len(hours) == 0 else hours[0]
    minutesText = "0" if len(minutes) == 0 else minutes[0]
    secondsText = "0" if len(seconds) == 0 else seconds[0]

    return "{} hora(s), {} minuto(s) y {} segundo(s).".format(hoursText, minutesText, secondsText)


def title(dom):
    header = ET.Element("header")
    h1 = ET.Element("h1")
    h1.text = dom.attrib["name"]
    header.append(h1)
    details = ET.Element("ul")
    prep = ET.Element("li")
    prep.text = "Tiempo de preparación: " + processDuration(dom.find("{http://www.uniovi.es}prep_time").text)
    details.append(prep)
    cook = ET.Element("li")
    cook.text = "Tiempo de cocinado: " + processDuration(dom.find("{http://www.uniovi.es}cook_time").text) 
    details.append(cook)
    diff = ET.Element("li")
    diff.text = "Dificultad: " + dom.find("{http://www.uniovi.es}difficulty").text
    details.append(diff)
    serv = ET.Element("li")
    serv.text = "Raciones: " +  dom.find("{http://www.uniovi.es}servings").text
    details.append(serv)
    header.append(details)
    return header

def ingredients(dom):
    section = ET.Element("section")
    h2 = ET.Element("h2")
    h2.text = "Ingredientes"
    section.append(h2)

    table = ET.Element("table")
    section.append(table)
    headers = ET.Element("tr")
    table.append(headers)
    header = ET.Element("th")
    header.text = "Ingrediente"
    headers.append(header)
    header = ET.Element("th")
    header.text = "Cantidad (kg)"
    headers.append(header)
    header = ET.Element("th")
    header.text = "Alergias"
    headers.append(header)
    
    for ingredient in dom.findall("{http://www.uniovi.es}ingredient"):
            row = ET.Element("tr")
            # Ingrediente
            data = ET.Element("td")
            data.text = ingredient.text
            row.append(data)

            # Cantidad
            data = ET.Element("td")
            data.text = ingredient.attrib["quantity"]
            row.append(data)

            # Alergias
            data = ET.Element("td")
            data.text = ingredient.attrib["alergy"]
            if(data.text == ""):
                data.text = "N/A"
            row.append(data)

            table.append(row)
            
    return section 

def steps(dom):
    section = ET.Element("section")
    h2 = ET.Element("h2")
    h2.text = "Pasos"
    section.append(h2)

    ol = ET.Element("ol")
    section.append(ol)
    for step in dom.findall("{http://www.uniovi.es}step"):
        li = ET.Element("li")
        ol.append(li)
        li.text = step.text
    return section

def notes(dom):
    section = ET.Element("section")
    h2 = ET.Element("h2")
    h2.text = "Notas"
    section.append(h2)

    ul = ET.Element("ul")
    for note in dom.findall("{http://www.uniovi.es}note"):
        li = ET.Element("li")
        ul.append(li)
        li.text = note.text

    if(len(ul) > 0):
        section.append(ul)

    return section 

def images(dom):
    section = ET.Element("section")
    h2 = ET.Element("h2")
    h2.text = "Imágenes"
    section.append(h2)


    for image in dom.findall("{http://www.uniovi.es}image"):
        img = ET.Element("img")
        img.attrib["src"] = image.attrib["url"]
        img.attrib["alt"] = image.text
        section.append(img)

    return section 

def tags(dom):
    section = ET.Element("section")
    h2 = ET.Element("h2")
    h2.text = "Etiquetas"
    section.append(h2)
    p = ET.Element("p")
    p.text = ""
    section.append(p)

    for tag in dom.findall("{http://www.uniovi.es}tag"):
        p.text += "#" + tag.text + " "
    return section 

def _add_doctype():
    # Literally because of the library
    #Greatest example of bodging
    line = '<!DOCTYPE html>'
    with open("recipee.html", 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + content)    

def validate(domPath):
    schema = etree.XMLSchema(etree.parse("recipee.xsd"))
    result = schema.validate(etree.parse(domPath))
    return result

def main(args):
    isValid = validate(args.inputfile)
    if(not isValid):
        raise Exception("Invalid recipee file")

    dom = parse_XML(args.inputfile)
    doc = ET.Element("html", attrib={"lang":"es"})
    doc.append(ET.Element("head"))
    doc.find("head").append(ET.Element("title"))
    doc.find("head").find("title").text = "Recetas LEW"
    doc.find("head").append(ET.Element("link", attrib={
        "rel":"stylesheet",
        "type":"text/css",
        "href":"style.css"
        }))
    doc.find("head").append(ET.Element("meta", attrib={ "CHARSET":"UTF-8"}))
    doc.find("head").append(ET.Element("meta", attrib={ 
        "name":"viewport",
        "content":"width=device-width, initial-scale=1.0"
        }))
    body = ET.Element("body")
    doc.append(body)
    header = title(dom)
    body.append(header)
    m = ET.Element("main")
    body.append(m)

    m.append(ingredients(dom.find("{http://www.uniovi.es}ingredients")))
    m.append(steps(dom.find("{http://www.uniovi.es}steps")))
    m.append(notes(dom.find("{http://www.uniovi.es}notes")))
    m.append(images(dom.find("{http://www.uniovi.es}images")))
    m.append(tags(dom.find("{http://www.uniovi.es}tags")))

    ET.ElementTree(doc).write(args.outputfile, encoding="UTF-8")
    _add_doctype()

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Procesa XML de recetas y genera una página web")
    parser.add_argument("inputfile", help="Fichero de entrada XML de recetas", type=str)
    parser.add_argument("outputfile", help="Fichero de salida HTML", type=str)
    main(parser.parse_args())