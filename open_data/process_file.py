import xml.etree.ElementTree as ET
import lxml.etree as etree
import argparse

def parse_XML(filename):
    ''' Given an filename for the XML, return the root
        Args:
            filename (str): the name of the XML file
        Returns:
            ElementTree.Element: the root of the XML
    '''
    try:
        tree = ET.parse(filename)
        return tree.getroot()
    except FileNotFoundError:
        print("No file found under name " + filename)
        exit(-1)

def row(dom):
    job = ET.Element("article")
    title = ET.Element("h2")
    title.text = dom.find("{https://www.oviedo.es}Expte").text + " - " + dom.find("{https://www.oviedo.es}TipoDeContrato").text
    job.append(title)
    date = ET.Element("p")
    date.text = "📅 "
    date.append(ET.Element("em"))
    date.find("em").text = dom.find("{https://www.oviedo.es}FechaJGL").text
    job.append(date)
    objective = ET.Element("p")
    objective.text = dom.find("{https://www.oviedo.es}ObjetoDeContrato").text
    job.append(objective)
    details = ET.Element("details")
    job.append(details)
    summary = ET.Element("summary")
    summary.text = "📑 Detalles del adjudicatario"
    details.append(summary)
    content = ET.Element("ul")
    name = ET.Element("li")
    name.text = "Nombre: " + dom.find("{https://www.oviedo.es}Adjudicatorio").text
    content.append(name)
    nif = ET.Element("li")
    nif.text = "CIF: " + dom.find("{https://www.oviedo.es}CIF").text
    content.append(nif)
    details.append(content)
    footer = ET.Element("footer")
    job.append(footer)
    amount = ET.Element("p")
    amount.text = "💸 "
    amountBold = ET.Element("strong")
    amountBold.text = dom.find("{https://www.oviedo.es}ImporteAdjudicacion").text
    amount.append(amountBold)
    footer.append(amount)
    process = ET.Element("p")
    process.text = dom.find("{https://www.oviedo.es}Procedimiento").text
    return job

def title():
    header = ET.Element("header")
    h1 = ET.Element("h1")
    h1.text = "Contratos por obras"
    header.append(h1)
    paragraph = ET.Element("p")
    paragraph.text = "A continuación se ofrecen los contratos por obras del Ayuntamiento de Oviedo."
    header.append(paragraph)
    return header

def _add_doctype():
    # Literally because of the library
    #Greatest example of bodging
    line = '<!DOCTYPE html>'
    with open("contabilidad.html", 'r+') as f:
        content = f.read()
        f.seek(0, 0)
        f.write(line.rstrip('\r\n') + '\n' + content)

def validate(domPath):
    schema = etree.XMLSchema(etree.parse("oviedo.xsd"))
    result = schema.validate(etree.parse(domPath))
    return result

def main(args):
    dom = parse_XML(args.inputfile)
    doc = ET.Element("html", attrib={"lang":"es"})
    doc.append(ET.Element("head"))
    doc.find("head").append(ET.Element("title"))
    doc.find("head").find("title").text = "Contratos por obras - Oviedo"
    doc.find("head").append(ET.Element("link", attrib={
        "rel":"stylesheet",
        "type":"text/css",
        "href":"style.css"
        }))
    doc.find("head").append(ET.Element("meta", attrib={ "CHARSET":"UTF-8"}))
    doc.find("head").append(ET.Element("meta", attrib={ 
        "name":"viewport",
        "content":"width=device-width, initial-scale=1.0"
        }))
    body = ET.Element("body")
    doc.append(body)
    header = title()
    body.append(header)
    m = ET.Element("main")
    body.append(m)
    for r in dom.findall("{https://www.oviedo.es}Row"):
        m.append(row(r))
    ET.ElementTree(doc).write(args.outputfile, encoding="UTF-8")
    _add_doctype()

if __name__ == "__main__":
    parser = argparse.ArgumentParser("Procesa XML de los contratos por obras de Oviedo y genera una página web")
    parser.add_argument("inputfile", help="Fichero de entrada XML de contratos", type=str)
    parser.add_argument("outputfile", help="Fichero de salida HTML", type=str)
    main(parser.parse_args())