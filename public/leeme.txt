El sitio web sigue una estructura clásica, esto es, con un header que contiene el título principal del sitio y la barra de navegación, un main con el contenido propio de la página en específico, y un footer dónde figuran meramente la autoría, y unas imágenes destacando la validez del css y el html.

Hay 4 páginas:
- index.html: página principal.
- aficiones.html: hobbys propios
- biblioteca.html: breve reseña de los libros que atesoro
- proyectos.html: proyectos informáticos propios

No se ha empleado ninguna carácterística o módulo en especial de CSS, pues se ha priorizado en todo momento la simplicidad.
Se han aplicado unas propias reglas de carácter general (y para tratar de mejorar la adaptabilidad) que se encuentran en el único archivo CSS
